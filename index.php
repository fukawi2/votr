<?php

// set our working directory to the base of index.php
chdir(__DIR__);

// Kickstart composer autoloader
require_once 'vendor/autoload.php';
$f3 = \Base::instance();
$f3->set('PACKAGE', 'votr');
$f3->set('AUTOLOAD', 'app/');
$f3->set('UI', 'ui/');
$f3->config('app/routes.cfg');

#$f3->set('DEBUG', 3);

// load config file
$f3->config('config.cfg');
$f3->config('config.local.cfg');

// initialize the database and session handler then start f3
$f3->set('DB', new \DB\SQL('sqlite:db/votr.sqlite'));
$f3->set('sess', new \DB\SQL\Session($f3->get('DB')));
$f3->run();

?>
