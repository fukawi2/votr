<?php

Class API extends Controller {

  function GetOptions($f3,$params) {
    $db_votes = new DB\SQL\Mapper($f3->get('DB'), 'votes');

    $granularity = ($f3->get('app.option_granularity') ?: '86400');
    if (is_numeric($granularity) === false or $granularity < 1)
      $f3->error(500, 'app.option_granularity is invalid');

    // we divide the last_vote column by $granularity to reduce the granularity
    // while still ensuring all options get presented reasonably often, but at
    // the same time not predjudicing the same songs to be presented against
    // each other.
    $db_votes->load(
      array('is_active=?', 1),  // filter
      array(
        'order' => "CAST(last_vote/$granularity AS INT) ASC, random()",
        'limit' => 2,
      ));

    // build array to be formatted as json back to client
    $response = array();
    while (!$db_votes->dry()) {
      $response[] = array(
        'id'    => $db_votes->id,
        'text'  => $db_votes->description,
      );
      $db_votes->next();
    }
    $response['token'] = $f3->get('SESSION.csrf_token');

    header('Content-Type: application/json;charset=utf-8');
    echo json_encode($response, JSON_PRETTY_PRINT);
  }



  function CastVote($f3,$params) {
    $winner_id = $f3->get('POST.winner');
    $loser_id = $f3->get('POST.loser');
    $last_vote_epoch = time();

    // update the winner
    $db_votes = new DB\SQL\Mapper($f3->get('DB'), 'votes');
    $db_votes->load(array('id=?', $winner_id));
    if ($db_votes->dry())
      $f3->error(404);
    $db_votes->frequency = $db_votes->frequency + 1;
    $db_votes->wins = $db_votes->wins + 1;
    $db_votes->last_vote = $last_vote_epoch;
    $db_votes->update();

    // update the loser
    $db_votes = new DB\SQL\Mapper($f3->get('DB'), 'votes');
    $db_votes->load(array('id=?', $loser_id));
    if ($db_votes->dry())
      $f3->error(404);
    $db_votes->frequency = $db_votes->frequency + 1;
    $db_votes->last_vote = $last_vote_epoch;
    $db_votes->update();

    http_response_code(204); // ok - no content
  }

}
