<?php

class Controller {

  const VERSION = '0.1';

  /*
   * fatfree calls this function on every execution before running the handler
   * in the relevant class. we use it to do some processing we always need,
   * such as loading global settings from the databae, csrf checks, and
   * checking user authentication. note that some classes override this
   * specific function with their own, in which case they explicitly call this
   * copy of beforeRoute() to ensure these things still happen.
   */
  function beforeRoute($f3,$params) {
    // Check csrf token. Refer: https://fatfreeframework.com/3.6/session#csrf
    if ($f3->VERB == 'POST' && $f3->exists('SESSION.csrf_token')) {
      // validate the csrf token
      if ($f3->get('POST.token') != $f3->get('SESSION.csrf_token'))
        $f3->error(400, 'Bad CSRF Token');
    }
    // create a new csrf token
    $f3->set('SESSION.csrf_token', $f3->get('sess')->csrf());

    // initialize database
    $this->__CreateTables();
  }


  /* This takes care of the final page rendering steps. Rather than repeating
   * these same lines in every controller function, we have this short helper
   * to reduce page rendering steps to a single function call.
   */
  public function RenderPage($content, $title, $header = null) {
    $f3 = Base::instance();
    $header = $header ?: $title; // default header to title if header not set
    $f3->set('PAGE.TITLE', $title);
    $f3->set('PAGE.HEADER', $header);
    $f3->set('PAGE.CONTENT', $content);
    echo \Template::instance()->render('layouts/default.htm');
  }


  private function __CreateTables() {
    $f3 = Base::instance();
    $commands = [
      'CREATE TABLE IF NOT EXISTS votes (
        id          INTEGER PRIMARY KEY,
        description TEXT,
        frequency   INTEGER DEFAULT 0,
        wins        INTEGER DEFAULT 0,
        is_active   INTEGER DEFAULT 1,
        last_vote   INTEGER DEFAULT 0
      )',
    ];
    // execute the sql commands to create new tables
    foreach ($commands as $command) {
      $f3->get('DB')->exec($command);
    }
  }

}
