<?php

Class Home extends Controller {

  const MAX_RESULTS = 10;


  function Index($f3,$params) {
    $db_votes = new DB\SQL\Mapper($f3->get('DB'), 'votes');

    $total_options = $db_votes->count();
    $total_combinations = $total_options * ($total_options-1);
    $f3->set('total_options', $total_options);
    $f3->set('total_combinations', $total_combinations);

    $this->RenderPage('home.htm', ($f3->get('app.title') ?: 'Home'), null);
  }


  function Results($f3,$params) {
    $db_votes = new DB\SQL\Mapper($f3->get('DB'), 'votes');
    $db_votes->losses = '(frequency - wins)';
    $db_votes->rating = '(wins*1.0 / frequency)'; // *1.0 to cast to float

    // get top X results
    $res = $db_votes->find(
      array('is_active=? AND frequency>0', 1),  // filter
      array(
        'order' => 'rating DESC, wins DESC',
        'limit' => self::MAX_RESULTS,
      ));
    $f3->set('res', $res);
    $db_votes->reset();

    // get most controversial
    $res = $db_votes->find(
      array('is_active=? AND rating>0.4 AND rating<0.6', 1),  // filter
      array(
        'order' => 'frequency DESC',
        'limit' => self::MAX_RESULTS,
      ));
    $f3->set('controversial', $res);
    $db_votes->reset();

    // get most and least favorite
    $db_votes->load(
      array('is_active=? AND frequency>0', 1),
      array(
        'order' => 'rating DESC, wins DESC',
        'limit' => 1
      )
    );
    $f3->set('top_result', $db_votes->cast());
    $db_votes->load(
      array('is_active=? AND frequency>0', 1),
      array(
        'order' => 'rating ASC, losses DESC',
        'limit' => 1
      )
    );
    $f3->set('bottom_result', $db_votes->cast());

    // get total vote count
    $db_votes->total_votes = 'SUM(frequency)';
    $db_votes->load();
    $f3->set('total_votes', $db_votes->total_votes);
    unset($db_votes->total_votes);
    $db_votes->reset();

    $this->RenderPage('results.htm', 'Results', null);
  }

}
