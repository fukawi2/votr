var current_option_ids = [];
var csrf_token;

var loading_msgs = [
  'Awesome choice!',
  'I like that one!',
  'Nice one!',
  'Great choice!',
  'My favorite!'
];

$(document).ready(function(){
  $('#jumbotron').collapse('show');
  loadVoteOptions();
});

$('#btn_CloseJumbotron').click(function(e) {
  $('#jumbotron').collapse('hide');
});

function loadVoteOptions() {
  $('#opt1_text').html('Loading...');
  $('#opt2_text').html('Loading...');

  $.ajax({
    url: "/api/opts/2",
    dataType: 'json',
    success: function(response){
      if (response[1]) {
        csrf_token = response['token'];
        var opt1 = response[0].text;
        var opt2 = response[1].text;
        current_option_ids[1] = response[0].id;
        $('#opt1_text').html(opt1);
        current_option_ids[2] = response[1].id;
        $('#opt2_text').html(opt2);
      }
    },
    error: function(xhr, textStatus, thrownError) {
      var newLine = "\r\n";
      var msg = "Sorry! Something went wrong.";
      msg += '('+xhr.status+' '+thrownError+')';
      msg += newLine;
      msg += newLine;
      msg += 'Try reloading this page.';
      alert(msg);
    }
  });

  // update the stars
  update_user_rating();
};

function do_vote(opt) {
  showLoading();

  var winner_key= (opt == 1 ? 1 : 2);
  var loser_key = (opt == 1 ? 2 : 1);
  var post_data = {};
  post_data['token']  = csrf_token;
  post_data['winner'] = current_option_ids[winner_key];
  post_data['loser']  = current_option_ids[loser_key];
  $.post({
    url: '/api/vote',
    data: post_data
  });
  // update saved count of votes
  var votecnt = localStorage.getItem('votecnt');
  votecnt = parseInt(votecnt, 10) + 1;
  if (isNaN(votecnt)) {
    votecnt = 1;
  }
  localStorage.setItem('votecnt', votecnt);

  loadVoteOptions();
  hideLoading();
};

function do_skip() {
  showLoading();
  loadVoteOptions();
  hideLoading();
};

function update_user_rating() {
  var votecnt = localStorage.getItem('votecnt');
  votecnt = parseInt(votecnt, 10);
  if (isNaN(votecnt)) {
    return;
  }

  if (votecnt > 999) {
    newhtml = buildStarHTML(5);
    newhtml += ' ' + votecnt + ' votes - You live for the music.';
  } else if (votecnt > 749) {
    newhtml = buildStarHTML(4);
    newhtml += ' ' + votecnt + " votes - You're a Music Affectionado.";
  } else if (votecnt > 499) {
    newhtml = buildStarHTML(3);
    newhtml += ' ' + votecnt + ' votes - What a Music Lover!';
  } else if (votecnt > 249) {
    newhtml = buildStarHTML(2);
    newhtml += ' ' + votecnt + ' votes - On a roll now...';
  } else if (votecnt > 99) {
    newhtml = buildStarHTML(1);
    newhtml += ' ' + votecnt + ' votes - Awesome effort!';
  } else {
    newhtml = buildStarHTML(0);
    newhtml += ' ' + votecnt + ' votes cast';
  }
  $('#user_rating').html(newhtml);
}

function buildStarHTML(cnt) {
  var html = "";
  for (i = 0; i < cnt; i++) {
    html += '<span class="fa fa-star checked"></span>';
  }
  for (i = 0; i < 5-cnt; i++) {
    html += '<span class="fa fa-star"></span>';
  }
  return html;
}

function showLoading() {
  var randomNumber = Math.floor(Math.random()*loading_msgs.length);
  $('#thx_msg').html(loading_msgs[randomNumber]);
  $("#divLoading").modal({
    backdrop: "static", //remove ability to close modal with click
    keyboard: false, //remove option to close with keyboard
    show: true //Display loader!
  });
};

function hideLoading() {
  $('#divLoading').on('shown.bs.modal', function (e) {
    $("#divLoading").modal('hide');
  })
}
